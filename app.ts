import * as MergeRequest from './src/merge-request';
import PostMergeRequests from './src/notification';
import { loadSettings } from './src/lib/configuration';
const { CONFIG_FILE } = process.env;

if (!CONFIG_FILE) {
    console.error("Please specify a CONFIG_FILE parameter to start");
    process.exit(0);
}

(async () => {
    await loadSettings(CONFIG_FILE);
    const mergeRequests = await MergeRequest.getOpenMergeRequests();
    const result = await PostMergeRequests(mergeRequests);
    console.info("Posted", mergeRequests.length, " Merge Requests. Successful posts:", result);
})();