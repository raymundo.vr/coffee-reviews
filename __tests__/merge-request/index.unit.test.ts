import fetch from 'node-fetch';
import { getOpenMergeRequests } from '../../src/merge-request';
import * as Configuration from '../../src/lib/configuration';
import { mocked } from 'ts-jest/utils';

const testURL = 'https://gitlab.tests.com/api/v4';
const testToken = 'this-is-not-a-token';

const simpleConfiguration = {
    gitlab: {
        base_url: testURL,
        token: testToken,
    },
    publish: {}
};

const projectsConfiguration = {
    gitlab: {
        base_url: testURL,
        token: testToken,
        projects: ['project-one', 'project-two', 'project-three'],
    },
    publish: {}
}

jest.mock('node-fetch');
jest.mock('../../src/lib/configuration');

describe("Get Open Merge Requests Behavior", () => {
    afterEach(() => {
        jest.resetAllMocks()
    });

    it("Should request all merge requests when no projects are specified", async () => {
        const mockedFetch = mocked(fetch as unknown as jest.Mock).mockImplementation(
            () => Promise.resolve({
                status: 200,
                json: jest.fn().mockResolvedValue([
                    {
                        title: 'test mr 1',
                        author: { name: 'tester' },
                        created_at: (new Date(Date.UTC(2021, 2, 27, 0, 0, 0))).toISOString(),
                        upvotes: 0,
                        url: `${testURL}/project/one/merge_requests/1`,
                    },
                    {
                        title: 'test mr 2',
                        author: { name: 'tester' },
                        created_at: (new Date(Date.UTC(2021, 2, 27, 0, 0, 0))).toISOString(),
                        upvotes: 1,
                        url: `${testURL}/project/two/merge_requests/1`,
                    }
                ])
            })
        );
        mocked(Configuration.getSettings).mockReturnValue(simpleConfiguration);

        const mrs = await getOpenMergeRequests();
        const expected = [
            {
                title: 'test mr 1',
                author: 'tester',
                date: '2021-03-27T00:00:00.000Z',
                upvotes: 0,
                url: undefined
            },
            {
                title: 'test mr 2',
                author: 'tester',
                date: '2021-03-27T00:00:00.000Z',
                upvotes: 1,
                url: undefined
            }
        ]

        expect(mrs).toEqual(expect.arrayContaining(expected));
        expect(mockedFetch).toHaveBeenCalledTimes(1);
        expect(mockedFetch).toHaveBeenCalledWith(
            `${testURL}/merge_requests?state=opened`,
            {
                headers: { 'PRIVATE-TOKEN': testToken },
                method: 'GET'
            }
        );
    });

    it("Should make request per project to get merge requests when projects is specified", async () => {
        const mockedFetch = mocked(fetch as unknown as jest.Mock).mockImplementation(
            () => Promise.resolve({ status: 200, json: jest.fn().mockResolvedValue([]) })
        );
        mocked(Configuration.getSettings).mockReturnValue(projectsConfiguration);

        const mrs = await getOpenMergeRequests();

        expect(mrs).toEqual(expect.arrayContaining([]));
        expect(mockedFetch).toHaveBeenCalledTimes(3);
    });

    it("Should throw an error when Gitlab API response is not successful", async () => {
        mocked(fetch as unknown as jest.Mock).mockImplementation(
            () => Promise.resolve({ status: 400, statusText: 'Bad Request' })
        );
        mocked(Configuration.getSettings).mockReturnValue(projectsConfiguration);
        // Jest expect(() => {}).toThrow() is not working as expected 
        try {
            expect(await getOpenMergeRequests()).toThrow('Cannot obtain merge requests: Bad Request');
            expect(false).toBe(true);
        } catch (err) {
            expect(err).toBeDefined();
        }
    });
});