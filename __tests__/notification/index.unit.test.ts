import fetch from 'node-fetch';
import * as Configuration from '../../src/lib/configuration';
import PublishMergeRequests from '../../src/notification';
import { mocked } from 'ts-jest/utils';
import { MergeRequest } from '../../src/common/types';

const PublishToSlackConfiguration = {
    gitlab: {
        base_url: 'https://gitlab.com/api/v4',
        token: 'this-is-not-a-token'
    },
    publish: {
        slack: {
            webhook_url: 'https://test.slack.app/webook',
        }
    }
}

jest.mock('node-fetch');
jest.mock('../../src/lib/configuration');

describe("Publish Merge Requests behavior", () => {
    afterEach(() => {
        jest.resetAllMocks();
    });

    it("Should post a message to Slack", async () => {
        const mockedFetch = (fetch as unknown as jest.Mock).mockResolvedValue(
            {
                status: 200,
                ok: true,
            }
        );

        mocked(Configuration.getSettings).mockReturnValue(PublishToSlackConfiguration);
        const mr: MergeRequest = {
            author: 'tester',
            date: '2021-03-28T00:00:00',
            title: 'Test MR',
            upvotes: 0,
            url: 'https://gitlab.com/api/v4/test/merge_requests/1'
        };

        const successPosts = await PublishMergeRequests([mr]);
        expect(mockedFetch).toHaveBeenCalledTimes(1);
        expect(successPosts).toEqual(1);
    });
});

