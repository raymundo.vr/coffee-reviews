import { MergeRequest } from '../common/types';
import { getSettings } from '../lib/configuration';
import fetch from 'node-fetch';

const getMergeRequestResponse = async (queryUrl: string, token: string): Promise<Array<any>> => {
    const TOKEN_HEADER = { 'PRIVATE-TOKEN': token };

    const mergeRequestsResponse = await fetch(queryUrl, {
        method: 'GET',
        headers: TOKEN_HEADER
    });

    if (mergeRequestsResponse.status !== 200) {
        throw new Error(`Cannot obtain merge requests: ${mergeRequestsResponse.statusText}`);
    }

    return await mergeRequestsResponse.json();
};

export const getOpenMergeRequests = async (): Promise<Array<MergeRequest>> => {
    const { gitlab } = getSettings();
    const includeWIP = gitlab.include_wip === undefined ? false : gitlab.include_wip;
    let mergeRequests: any[] = [];

    if (!Array.isArray(gitlab.projects)) {
        mergeRequests = await getMergeRequestResponse(`${gitlab.base_url}/merge_requests?state=opened`, gitlab.token);
    } else {
        const allMrs = await Promise.all(
            gitlab.projects.map(projectId => getMergeRequestResponse(
                `${gitlab.base_url}/projects/${projectId}/merge_requests?state=opened`,
                gitlab.token
            )
            )
        );
        mergeRequests = allMrs.flat();
    }

    return mergeRequests
        .filter((mr: any) => includeWIP ? true : !mr.work_in_progress)
        .map((mr: any) => ({
            title: mr.title,
            author: mr.author.name,
            date: mr.created_at,
            upvotes: mr.upvotes,
            url: mr.web_url,
        }));
};