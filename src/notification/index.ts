import fetch from 'node-fetch';
import { MergeRequest, WebhookAPI } from '../common/types';
import { getSettings } from '../lib/configuration';

const logger = console;

const formatDate = (dateString: string) => (new Date(dateString)).toLocaleString()

const formatMrkdwn = (mergeRequest: MergeRequest): string =>
    `- <${mergeRequest.url}|${mergeRequest.title}> by ${mergeRequest.author}. Opened on *${formatDate(mergeRequest.date)}*. Upvotes: ${mergeRequest.upvotes}`;

const formatMarkdown = (mergeRequest: MergeRequest): string =>
    `- [${mergeRequest.title}](${mergeRequest.url}) by ${mergeRequest.author}. Opened on __${formatDate(mergeRequest.date)}__. Upvotes: ${mergeRequest.upvotes}`;

const slackPayload = (mergeRequestsMessages: string[], salutation: string): object => ({
    text: `${salutation}\n${mergeRequestsMessages.join('\n')}`
});

const teamsPayload = (mergeRequestsMessages: string[], salutation: string): object => ({
    title: salutation,
    text: `${mergeRequestsMessages.join('\r')}`
});

const publishToWebhook = async (webhookUrl: string, message: object): Promise<boolean> => {
    const response = await fetch(webhookUrl, {
        headers: {
            'Content-Type': 'application/json'
        },
        method: 'POST',
        body: JSON.stringify(message)
    });

    if (!response.ok) {
        logger.error("Cannot post message! Received:", response.statusText);
        return false;
    }

    return true;
};

const publishMergeRequests = async (mergeRequests: MergeRequest[]): Promise<number> => {
    const { publish } = getSettings();
    const channels = Object.keys(publish);
    const salutation = "Hi! There are some Merge Requests to review :)"
    let successulfCount = 0;
    let apiInfo: WebhookAPI;
    let mrMessages: string[];
    let message: object;

    for (const channel of channels) {
        switch (channel) {
            case "slack":
                apiInfo = publish.slack as WebhookAPI;
                mrMessages = mergeRequests.map(mr => formatMrkdwn(mr));
                message = slackPayload(mrMessages, salutation);
                if (await publishToWebhook(apiInfo.webhook_url, message)) {
                    successulfCount++;
                }
                break;

            case "teams":
                apiInfo = publish.teams as WebhookAPI;
                mrMessages = mergeRequests.map(mr => formatMarkdown(mr));
                message = teamsPayload(mrMessages, salutation);
                if (await publishToWebhook(apiInfo.webhook_url, message)) {
                    successulfCount++;
                }
                break;
        }
    }

    return successulfCount;
};

export default publishMergeRequests;