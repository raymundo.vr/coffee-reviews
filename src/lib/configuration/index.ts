import { Configuration } from "../../common/types";
import { promisify } from 'util';
import { readFile } from 'fs';

const readFilePromise = promisify(readFile)

let ConfigurationSettings: Configuration;

export const loadSettings = async (filePath: string) => {
    const fileContents = await readFilePromise(filePath);
    ConfigurationSettings = JSON.parse(
        fileContents.toString()
    );
};

export const getSettings = (): Configuration => ConfigurationSettings;