/**
 * Project types, interfaces, enums and constants
 */

export interface MergeRequest {
    title: string;
    author: string;
    date: string;
    upvotes: number;
    url: string;
}

export interface GitlabConfiguration {
    base_url: string;
    token: string;
    include_wip?: boolean;
    projects?: string[];
}

export interface WebhookAPI {
    webhook_url: string;
}
export interface PublishConfiguration {
    slack?: WebhookAPI;
    teams?: WebhookAPI;
}

export interface Configuration {
    gitlab: GitlabConfiguration;
    publish: PublishConfiguration;
}